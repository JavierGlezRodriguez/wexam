import { Problema } from './problema';
import { Profesor } from './profesor';

export class Circulo {
    id: number;
    nombre: string;
    creador: Profesor;
    problemas: Problema[];
    profesores: Profesor[];
}