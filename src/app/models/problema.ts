import { Pregunta } from './pregunta';
import { Profesor } from './profesor';
import { Circulo } from './circulo';

export class Problema {
    id: number;
    resumen: string;
    enunciado: string;
    tags: string[];
    cuestiones: Pregunta[];
    n_cuestiones: number;
    puntos: number;
    creador: Profesor;
    es_borrable: boolean;
    es_compartible: boolean;
    compartido: Circulo[];
}