export class Pregunta {
    puntos: number;
    enunciado: string;
    respuesta: string;
    explicacion: string;
}