import { Problema } from './problema';

export class Examen {
    id: number;
    estado: string;
    asignatura: string;
    titulacion: string;
    fecha: string;
    convocatoria: string;
    intro: string;
    problemas: Problema[];
    total_puntos: number;
}