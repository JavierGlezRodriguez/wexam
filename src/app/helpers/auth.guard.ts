import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

// Esta clase protege la navegación asegurandose de que exista un token de login y no esté caducado 
@Injectable()
export class AuthGuard implements CanActivate {

    helper = new JwtHelperService();

    constructor(
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (sessionStorage.getItem('token')) {
            // Si existe token comprobamos que no haya expirado
            let token = JSON.parse(sessionStorage.getItem('token'));
            if (!this.helper.isTokenExpired(token))
                // logueado y con token sin caducar por lo que retornamos true
                return true;
            else {
                // token expirado por lo que redirigimos al login y retornamos false
                this.router.navigate(['/login']);
                return false;
            }
        }

        // no logueado por lo que redirigimos al login y retornamos false
        this.router.navigate(['/login']);
        return false;
    }
}