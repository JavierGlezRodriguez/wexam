import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '../componentes/login/login.component'

import { ProblemasAddComponent } from '../componentes/problemas-add/problemas-add.component';
import { ProblemasListComponent } from '../componentes/problemas-list/problemas-list.component';
import { ProblemasEditComponent } from '../componentes/problemas-edit/problemas-edit.component';
import { ProblemasViewComponent } from '../componentes/problemas-view/problemas-view.component';

import { ExamenesListComponent } from '../componentes/examenes-list/examenes-list.component';
import { ExamenesAddComponent } from '../componentes/examenes-add/examenes-add.component';
import { ExamenesEditComponent } from '../componentes/examenes-edit/examenes-edit.component';
import { ExamenesDetailComponent } from '../componentes/examenes-detail/examenes-detail.component';
import { ExamenesViewComponent } from '../componentes/examenes-view/examenes-view.component';

import { CirculosListComponent } from '../componentes/circulos-list/circulos-list.component';
import { CirculosDetailComponent } from '../componentes/circulos-detail/circulos-detail.component';

import { AuthGuard } from '../helpers/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/problemas-list', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'problemas-list', component: ProblemasListComponent, canActivate: [AuthGuard] },
  { path: 'problemas-add', component: ProblemasAddComponent, canActivate: [AuthGuard] },  
  { path: 'problemas-edit/:id', component: ProblemasEditComponent, canActivate: [AuthGuard] }, 
  { path: 'problemas-view/:id', component: ProblemasViewComponent, canActivate: [AuthGuard] }, 
  { path: 'examenes-list', component: ExamenesListComponent, canActivate: [AuthGuard] },
  { path: 'examenes-add', component: ExamenesAddComponent, canActivate: [AuthGuard] },
  { path: 'examenes-edit/:id', component: ExamenesEditComponent, canActivate: [AuthGuard] }, 
  { path: 'examenes-detail/:id', component: ExamenesDetailComponent, canActivate: [AuthGuard] },
  { path: 'examenes-view/:id', component: ExamenesViewComponent, canActivate: [AuthGuard] }, 
  { path: 'circulos-list', component: CirculosListComponent, canActivate: [AuthGuard] },
  { path: 'circulos-detail/:id', component: CirculosDetailComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
