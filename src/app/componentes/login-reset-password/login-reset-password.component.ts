import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login-reset-password',
  templateUrl: './login-reset-password.component.html',
  styleUrls: ['./login-reset-password.component.css']
})
export class LoginResetPasswordComponent implements OnInit {

  resetForm: FormGroup;

  formErrors = {
    'email': ''
  };

  validationMessages = {
    'email': {
      'required': 'El campo email es obligatorio.',
      'email': 'El campo email debe ser un email válido.'
    }
  };

  constructor(
    private dataService: DataService,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.resetForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]]
    });

    this.resetForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.resetForm) { return; }
    const form = this.resetForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    this.dataService.resetPassword(this.resetForm.value.email.toLocaleLowerCase()).subscribe(
      data => {
        this.activeModal.dismiss('Mail sent');
        alert("Correo enviado.");
      },
      error => {
        if (error.status == 404) {
          alert("El email introducido no está registrado en la aplicación.");
        }
        else {
          alert("Error desconocido, es posible que el servidor esté caido, vuelva a intentarlo más tarde.");
        }
      });
  }

}
