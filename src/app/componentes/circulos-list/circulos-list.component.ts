import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Circulo } from '../../models/circulo';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';
import { PagerService } from '../../services/pager.service';

import { CirculosAddComponent } from '../circulos-add/circulos-add.component';
import { CirculosEditComponent } from '../circulos-edit/circulos-edit.component';

@Component({
  selector: 'app-circulos-list',
  templateUrl: './circulos-list.component.html',
  styleUrls: ['./circulos-list.component.css']
})

export class CirculosListComponent implements OnInit {

  circulos: Circulo[];
  circulosFiltrados: Circulo[];
  buscar: string = sessionStorage.getItem('buscar_circulosList');

  // pager object
  pager: any = {};
  // current page of items
  pageOfCirculos: Circulo[];

  constructor(
    private alertService: AlertService,
    private dataService: DataService,
    private modalService: NgbModal,
    private pagerService: PagerService
  ) { }

  ngOnInit() {
    this.getCirculos();
  }

  getCirculos(): void {
    this.dataService.getCirculos()
      .subscribe(
        circulos => {
          this.circulos = circulos;
          this.assignCopy();
          this.search(this.buscar);
        },
        error => {
          if (error.status == 0) {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
  }

  assignCopy() {
    this.circulosFiltrados = Object.assign([], this.circulos);
    this.setPage(1);
  }
  
  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.circulosFiltrados.length, page);

    // get current page of items
    this.pageOfCirculos = this.circulosFiltrados.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  onChangePage(pageOfCirculos: Circulo[]) {
    // update current page of items
    this.pageOfCirculos = pageOfCirculos;
  }

  deleteCirculo(circulo: Circulo): void {
    if (confirm("¿Está seguro de querer borrar " + circulo.nombre + "?")) {
      this.circulos = this.circulos.filter(h => h !== circulo);
      this.dataService.deleteCirculo(circulo).subscribe(data => this.search(this.buscar));
    }
  }

  openAdd() {
    const modalRef = this.modalService.open(CirculosAddComponent);

    modalRef.result.then((result) => {
      //console.log(result);
      this.addCirculo(result);
    }).catch((error) => {
      //console.log(error);
    });
  }

  addCirculo(nombreCirculo: string): void {
    let circulo = new Circulo();
    circulo.nombre = nombreCirculo;
    //Create Circulo
    this.dataService.addCirculo(circulo)
      .subscribe(() => this.getCirculos());
  }

  openEdit(id: number) {
    const modalRef = this.modalService.open(CirculosEditComponent);
    modalRef.componentInstance.id = id;

    modalRef.result.then((result) => {
      //console.log(result);
      this.editCirculo(result, id);
    }).catch((error) => {
      //console.log(error);
    });
  }

  editCirculo(circulo: Circulo, idCirculo: number): void {
    circulo.id = idCirculo;
    //Update Circulo
    this.dataService.updateCirculo(circulo)
      .subscribe(() => this.getCirculos());
  }

  search(buscar) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    if (buscar != null) {
      sessionStorage.setItem('buscar_circulosList', buscar);
      this.buscar = buscar;
    }
    else{
      sessionStorage.setItem('buscar_circulosList', "");
      this.buscar = "";
    }

    this.circulosFiltrados = Object.assign([], this.circulos).filter(
      circulo => buscar.toLowerCase().split(" ").every((elem) => circulo.nombre.toLowerCase().includes(elem)));

    this.setPage(1);
  }

}
