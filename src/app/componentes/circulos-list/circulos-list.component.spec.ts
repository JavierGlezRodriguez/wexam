import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CirculosListComponent } from './circulos-list.component';

describe('CirculosListComponent', () => {
  let component: CirculosListComponent;
  let fixture: ComponentFixture<CirculosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CirculosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CirculosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
