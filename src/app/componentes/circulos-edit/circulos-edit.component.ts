import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Circulo } from '../../models/circulo';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-circulos-edit',
  templateUrl: './circulos-edit.component.html',
  styleUrls: ['./circulos-edit.component.css']
})
export class CirculosEditComponent implements OnInit {

  @Input() id: number;
  circuloForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private dataService: DataService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.getCirculo();
    this.createForm();
  }

  createForm() {
    this.circuloForm = this.fb.group({
      nombre: ''
    });
  }

  getCirculo(): void {
    const id = this.id;
    this.dataService.getCirculo(id)
      .subscribe(circulo => this.rebuildForm(circulo));
  }

  rebuildForm(circulo: Circulo) {
    this.circuloForm.reset({
      nombre: circulo.nombre
    });    
  }

  onSubmit() {
    this.activeModal.close(this.circuloForm.value);
  }

}
