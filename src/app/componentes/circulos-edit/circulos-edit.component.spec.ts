import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CirculosEditComponent } from './circulos-edit.component';

describe('CirculosEditComponent', () => {
  let component: CirculosEditComponent;
  let fixture: ComponentFixture<CirculosEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CirculosEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CirculosEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
