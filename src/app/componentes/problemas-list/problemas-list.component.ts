import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Problema } from '../../models/problema';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';
import { PagerService } from '../../services/pager.service';

import { ProblemasShareComponent } from '../problemas-share/problemas-share.component';

@Component({
  selector: 'app-problemas-list',
  templateUrl: './problemas-list.component.html',
  styleUrls: ['./problemas-list.component.css']
})
export class ProblemasListComponent implements OnInit {

  problemas: Problema[];
  problemasFiltrados: Problema[];

  // pager object
  pager: any = {};
  // current page of items
  pageOfProblemas: Problema[];

  buscar: string = sessionStorage.getItem('buscar_problemasList');
  campo: string = sessionStorage.getItem('campo_problemasList');
  campos = ['todo', 'resumen', 'enunciado', 'creador', 'tags'];

  constructor(
    private alertService: AlertService,
    private dataService: DataService,
    private modalService: NgbModal,
    private pagerService: PagerService
  ) { }

  ngOnInit() {
    this.getProblemas();
  }

  getProblemas(): void {
    this.dataService.getProblemas()
      .subscribe(
        problemas => {
          this.problemas = problemas;
          this.assignCopy();
          this.search(this.buscar, this.campo);
        },
        error => {
          if (error.status == 0) {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
  }

  delete(problema: Problema): void {
    if (confirm("¿Está seguro de querer borrar " + problema.resumen + "? Se eliminará de los círculos y exámenes abiertos en los que esté")) {
      this.problemas = this.problemas.filter(h => h !== problema);
      this.dataService.deleteProblema(problema).subscribe(
        any => {
          this.search(this.buscar, this.campo);
        },
        error => {
          if (error.status == 422) {
            this.alertService.error("El problema no ha pododido ser eliminado, seguramente se haya añadido a un examen cerrado o publicado");
            this.getProblemas();
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
    }
  }

  clone(problema: Problema): void {
    if (confirm("Esto creará un duplicado del problema '" + problema.resumen + "' que pasará a ser de su propiedad ¿Ok?")) {
      this.problemas = this.problemas.filter(h => h !== problema);
      this.dataService.cloneProblema(problema).subscribe(
        any => {
          this.getProblemas();
        },
        error => {
          if (error.status == 403) {
            this.alertService.error("La clonación no ha podido completarse con éxito, es posible que el problema haya dejado de compartirse con usted. Se ha refrescado la lista");
            this.getProblemas();
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
    }
  }

  openShare(id: number) {
    const modalRef = this.modalService.open(ProblemasShareComponent);
    modalRef.componentInstance.id = id;
  }

  assignCopy() {
    this.problemasFiltrados = Object.assign([], this.problemas);
    this.setPage(1);
  }

  search(buscar, campo) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    if (buscar != null && campo != null) {
      sessionStorage.setItem('buscar_problemasList', buscar);
      sessionStorage.setItem('campo_problemasList', campo);
      this.buscar = buscar;
      this.campo = campo;
    }
    else {
      sessionStorage.setItem('buscar_problemasList', "");
      sessionStorage.setItem('campo_problemasList', "todo");
      this.buscar = "";
      this.campo = "todo";
    }

    if (campo == 'todo') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem) ||
          problema.snippet.toLowerCase().includes(elem) ||
          problema.creador.nombre.toLowerCase().includes(elem) ||
          problema.tags.some((tag) => tag.toLowerCase().includes(elem))
        )
      )
    }

    if (campo == 'resumen') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem))
      )
    }
    if (campo == 'enunciado') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.snippet.toLowerCase().includes(elem))
      )
    }
    if (campo == 'creador') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.creador.nombre.toLowerCase().includes(elem))
      )
    }
    if (campo == 'tags') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.tags.some((tag) => {
          if (elem.includes('"')) {
            return elem.replace(/"/g, '').toLowerCase() === tag;
          }
          return tag.toLowerCase().includes(elem);
        }))
      )
    }

    this.setPage(1);
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.problemasFiltrados.length, page);

    // get current page of items
    this.pageOfProblemas = this.problemasFiltrados.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  spin(event) {
    event.target.classList.add('fa-spin'); // To ADD
    setTimeout(function () { event.target.classList.remove('fa-spin'); }, 1000);
  }

}
