import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemasListComponent } from './problemas-list.component';

describe('ProblemasListComponent', () => {
  let component: ProblemasListComponent;
  let fixture: ComponentFixture<ProblemasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
