import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CirculosAddComponent } from './circulos-add.component';

describe('CirculosAddComponent', () => {
  let component: CirculosAddComponent;
  let fixture: ComponentFixture<CirculosAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CirculosAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CirculosAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
