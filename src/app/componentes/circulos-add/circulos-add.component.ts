import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';


@Component({
  selector: 'app-circulos-add',
  templateUrl: './circulos-add.component.html',
  styleUrls: ['./circulos-add.component.css']
})
export class CirculosAddComponent implements OnInit {

  circuloForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.circuloForm = this.fb.group({
      nombre: ''
    });
  }

  onSubmit() {
    this.activeModal.close(this.circuloForm.value);
  }

}
