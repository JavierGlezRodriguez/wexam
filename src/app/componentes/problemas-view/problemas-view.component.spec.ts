import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemasViewComponent } from './problemas-view.component';
import { NavbarComponent } from '../navbar/navbar.component';


describe('ProblemasViewComponent', () => {
  let component: ProblemasViewComponent;
  let fixture: ComponentFixture<ProblemasViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemasViewComponent, NavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemasViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
