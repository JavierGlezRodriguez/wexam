import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { Problema } from '../../models/problema';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';

import { ProblemasShareComponent } from '../problemas-share/problemas-share.component';

@Component({
  selector: 'app-problemas-view',
  templateUrl: './problemas-view.component.html',
  styleUrls: ['./problemas-view.component.css']
})
export class ProblemasViewComponent implements OnInit {

  problema: Problema;

  constructor(
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.getProblema();
  }

  getProblema(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getProblema(id)
      .subscribe(
        problema => {
          this.problema = problema;
        },
        error => {
          if (error.status == 403) {
            this.alertService.error("El problema no está disponible para usted, es posible que se lo hayan dejado de compartir");
          }
          if (error.status == 404) {
            this.gotoList();
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
  }

  delete(problema: Problema): void {
    if (confirm("¿Está seguro de querer borrar " + problema.resumen + "? Se eliminará de los círculos y exámenes abiertos en los que esté")) {
      this.dataService.deleteProblema(problema).subscribe(
        any => {
          this.gotoList();
        },
        error => {
          if (error.status == 422) {
            this.alertService.error("El problema no ha pododido ser eliminado, seguramente se haya añadido a un examen cerrado o publicado");
            this.getProblema();
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
    }
  }

  clone(problema: Problema): void {
    if (confirm("Esto creará un duplicado del problema '" + problema.resumen + "' que pasará a ser de su propiedad ¿Ok?")) {
      this.dataService.cloneProblema(problema).subscribe(
        any => {
          alert("Problema clonado");
        },
        error => {
          if (error.status == 403) {
            this.alertService.error("La clonación no ha podido completarse con éxito, es posible que el problema haya dejado de compartirse con usted");
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
    }
  }

  openShare(id: number) {
    const modalRef = this.modalService.open(ProblemasShareComponent);
    modalRef.componentInstance.id = id;
  }

  gotoList() {
    this.router.navigate(['/problemas-list']);
  }

}
