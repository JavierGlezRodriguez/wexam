import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';

import { LoginResetPasswordComponent } from '../login-reset-password/login-reset-password.component';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    loginForm: FormGroup;

    formErrors = {
        'email': '',
        'password': ''
    };

    validationMessages = {
        'email': {
            'required': 'El campo email es obligatorio.',
            'email': 'El campo email debe ser un email válido.'
        },
        'password': {
            'required': 'El campo password es obligatorio.'
        }
    };

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private dataService: DataService,
        private alertService: AlertService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit() {
        this.createForm();

        //reset login status
        this.dataService.logout();

    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', [
                Validators.required,
                Validators.email
            ]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // (re)set validation messages now
    }

    onValueChanged(data?: any) {
        if (!this.loginForm) { return; }
        const form = this.loginForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        var email = this.f.email.value as string;
        email = email.toLocaleLowerCase();
        this.dataService.login(email, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/problemas-list']);
                },
                error => {
                    if (error.status == 401) {
                        this.alertService.error("El usuario o contraseña son incorrectos");
                    }
                    else {
                        this.alertService.error("Error desconocido, es posible que el servidor esté caido");
                    }
                });
    }

    openReset() {
        this.modalService.open(LoginResetPasswordComponent);
    }
}