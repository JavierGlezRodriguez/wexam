import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../services/data.service';

import { Circulo } from '../../models/circulo';
import { Profesor } from '../../models/profesor';
import { Problema } from '../../models/problema';


@Component({
  selector: 'app-circulos-detail',
  templateUrl: './circulos-detail.component.html',
  styleUrls: ['./circulos-detail.component.css']
})
export class CirculosDetailComponent implements OnInit {

  circulo: Circulo;
  profesores: Profesor[];
  miembros: Profesor[];
  problemas: Problema[];
  problemasFiltrados: Problema[];
  problemasCirculo: Problema[];
  problemasCirculoFiltrados: Problema[];
  idCirculo = +this.route.snapshot.paramMap.get('id');

  buscar: string = sessionStorage.getItem('buscar_circulosDetail');
  campo: string = sessionStorage.getItem('campo_circulosDetail');
  campos = ['todo', 'resumen', 'enunciado', 'creador', 'tags'];

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService
  ) {
  }

  ngOnInit() {
    this.getCirculo();
  }

  getCirculo(): void {
    this.dataService.getCirculo(this.idCirculo)
      .subscribe(circulo => {
        this.circulo = circulo;
        this.getListasProfesores();
        this.getListasProblemas();
      });
  }

  getListasProfesores(): void {
    this.dataService.getProfesores()
      .subscribe(profesores => this.dataService.getMiembros(this.idCirculo)
        .subscribe(miembros => {
          this.profesores = profesores.filter(item => !miembros.some(other => item.id === other.id));
          this.profesores = this.profesores.filter(profesor => profesor.id != this.circulo.creador.id);
          this.miembros = miembros;
        })
      );
  }

  addMiembro(miembro: Profesor): void {
    this.dataService.addMiembro(this.idCirculo, miembro.id)
      .subscribe(any => this.getListasProfesores());
  }

  removeMiembro(miembro: Profesor): void {
    this.dataService.removeMiembro(this.idCirculo, miembro.id)
      .subscribe(any => this.getListasProfesores());
  }

  getListasProblemas(): void {
    this.dataService.getProblemas()
      .subscribe(problemas => this.dataService.getProblemasCirculo(this.idCirculo)
        .subscribe(problemasCirculo => {
          this.problemas = problemas.filter(item => !problemasCirculo.some(other => item.id === other.id));
          this.problemas = this.problemas.filter(problema => problema.creador.id == this.circulo.creador.id);
          this.problemasCirculo = problemasCirculo;
          this.assignCopy();
          this.search(this.buscar, this.campo);
        })
      );
  }

  addProblema(problema: Problema): void {
    this.dataService.addProblemaCirculo(this.idCirculo, problema.id)
      .subscribe(any => this.getListasProblemas());
  }

  removeProblema(problema: Problema): void {
    this.dataService.removeProblemaCirculo(this.idCirculo, problema.id)
      .subscribe(any => this.getListasProblemas());
  }

  assignCopy() {
    this.problemasFiltrados = Object.assign([], this.problemas);
    this.problemasCirculoFiltrados = Object.assign([], this.problemasCirculo);
  }

  /*
  search(buscar, campo) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    this.buscar = buscar;
    this.campo = campo;

    if (campo == 'todo') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem) ||
          problema.enunciado.toLowerCase().includes(elem) ||
          problema.creador.nombre.toLowerCase().includes(elem) ||
          problema.tags.some((tag) => tag.toLowerCase().includes(elem))
        )
      )
    }

    if (campo == 'resumen') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem))
      )
    }
    if (campo == 'enunciado') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.enunciado.toLowerCase().includes(elem))
      )
    }
    if (campo == 'creador') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.creador.nombre.toLowerCase().includes(elem))
      )
    }
    if (campo == 'tags') {
      this.problemasFiltrados = Object.assign([], this.problemas).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.tags.some((tag) => tag.toLowerCase().includes(elem)))
      )
    }
  }*/

  update_search_results(buscar, campo, dst, src) {
    if (campo == 'todo') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem) ||
          problema.snippet.toLowerCase().includes(elem) ||
          problema.creador.nombre.toLowerCase().includes(elem) ||
          problema.tags.some((tag) => tag.toLowerCase().includes(elem))
        )
      )
    }

    if (campo == 'resumen') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem))
      )
    }
    if (campo == 'enunciado') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.snippet.toLowerCase().includes(elem))
      )
    }
    if (campo == 'creador') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.creador.nombre.toLowerCase().includes(elem))
      )
    }
    if (campo == 'tags') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.tags.some((tag) => tag.toLowerCase().includes(elem)))
      )
    }
  }

  search(buscar, campo) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    if (buscar != null && campo != null) {
      sessionStorage.setItem('buscar_circulosDetail', buscar);
      sessionStorage.setItem('campo_circulosDetail', campo);
      this.buscar = buscar;
      this.campo = campo;
    }
    else {
      sessionStorage.setItem('buscar_circulosDetail', "");
      sessionStorage.setItem('campo_circulosDetail', "todo");
      this.buscar = "";
      this.campo = "todo";
    }

    this.update_search_results(buscar, campo, "problemasFiltrados", "problemas");
    this.update_search_results(buscar, campo, "problemasCirculoFiltrados", "problemasCirculo");
  }

  spin(event) {
    event.target.classList.add('fa-spin'); // To ADD
    setTimeout(function () { event.target.classList.remove('fa-spin'); }, 1000);
  }

}
