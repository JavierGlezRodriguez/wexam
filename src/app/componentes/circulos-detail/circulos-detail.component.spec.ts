import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CirculosDetailComponent } from './circulos-detail.component';

describe('CirculosDetailComponent', () => {
  let component: CirculosDetailComponent;
  let fixture: ComponentFixture<CirculosDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CirculosDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CirculosDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
