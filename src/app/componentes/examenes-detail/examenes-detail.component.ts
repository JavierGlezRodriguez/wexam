import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../services/data.service';

import { Examen } from '../../models/examen';
import { Problema } from '../../models/problema';

@Component({
  selector: 'app-examenes-detail',
  templateUrl: './examenes-detail.component.html',
  styleUrls: ['./examenes-detail.component.css']
})
export class ExamenesDetailComponent implements OnInit {

  examen: Examen;
  problemas: Problema[];
  problemasFiltrados: Problema[];
  problemasExamen: Problema[];
  problemasExamenFiltrados: Problema[];
  idExamen = +this.route.snapshot.paramMap.get('id');

  buscar: string = sessionStorage.getItem('buscar_examenesDetail');
  campo: string = sessionStorage.getItem('campo_examenesDetail');
  campos = ['todo', 'resumen', 'enunciado', 'creador', 'tags'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.getExamen();
  }

  getExamen(): void {
    this.dataService.getExamen(this.idExamen)
      .subscribe(examen => {
        if (examen.estado != 'abierto') {
          this.router.navigate(['/examenes-list']);
        }
        else {
          this.examen = examen
          this.getListasProblemas();
        }
      });
  }

  getListasProblemas(): void {
    this.dataService.getProblemas()
      .subscribe(problemas => this.dataService.getProblemasExamen(this.idExamen)
        .subscribe(problemasExamen => {
          this.problemas = problemas.filter(item => !problemasExamen.some(other => item.id === other.id))
          this.problemasExamen = problemasExamen;
          this.examen.total_puntos = this.problemasExamen.reduce((a,b)=> a + b.puntos, 0);
          this.assignCopy();
          this.search(this.buscar, this.campo);
        })
      );
  }

  addProblema(problema: Problema): void {
    this.dataService.addProblemaExamen(this.idExamen, problema.id)
      .subscribe(any => this.getListasProblemas());
  }

  removeProblema(problema: Problema): void {
    this.dataService.removeProblemaExamen(this.idExamen, problema.id)
      .subscribe(any => this.getListasProblemas());
  }

  assignCopy() {
    this.problemasFiltrados = Object.assign([], this.problemas);
    this.problemasExamenFiltrados = Object.assign([], this.problemasExamen);
  }

  update_search_results(buscar, campo, dst, src) {
    if (campo == 'todo') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem) ||
          problema.snippet.toLowerCase().includes(elem) ||
          problema.creador.nombre.toLowerCase().includes(elem) ||
          problema.tags.some((tag) => tag.toLowerCase().includes(elem))
        )
      )
    }

    if (campo == 'resumen') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.resumen.toLowerCase().includes(elem))
      )
    }
    if (campo == 'enunciado')  {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.snippet.toLowerCase().includes(elem))
      )
    }
    if (campo == 'creador') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.creador.nombre.toLowerCase().includes(elem))
      )
    }
    if (campo == 'tags') {
      this[dst] = Object.assign([], this[src]).filter(
        problema => buscar.toLowerCase().split(" ").every((elem) => problema.tags.some((tag) => {
          if (elem.includes('"')) {
            return elem.replace(/\"/g, '').toLowerCase() === tag;
          }
          return tag.toLowerCase().includes(elem); 
        }))
      )
    }
  }

  search(buscar, campo) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    if (buscar != null && campo != null) {
      sessionStorage.setItem('buscar_examenesDetail', buscar);
      sessionStorage.setItem('campo_examenesDetail', campo);
      this.buscar = buscar;
      this.campo = campo;
    }
    else{
      sessionStorage.setItem('buscar_examenesDetail', "");
      sessionStorage.setItem('campo_examenesDetail', "tags");
      this.buscar = "";
      this.campo = "tags";
    }

    this.update_search_results(buscar, campo, "problemasFiltrados", "problemas");
    this.update_search_results(buscar, campo, "problemasExamenFiltrados", "problemasExamen");
  }

  spin(event){
    event.target.classList.add('fa-spin'); // To ADD
    setTimeout(function() { event.target.classList.remove('fa-spin'); }, 1000);
  }

}
