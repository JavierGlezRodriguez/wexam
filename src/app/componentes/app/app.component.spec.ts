import { TestBed, async } from '@angular/core/testing';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';

import { LoginComponent } from '../login/login.component';
import { AlertComponent } from '../alert/alert.component';

import { NavbarComponent } from '../navbar/navbar.component';
import { MessagesComponent } from '../messages/messages.component';

import { ProblemasAddComponent } from '../problemas-add/problemas-add.component';
import { ProblemasListComponent } from '../problemas-list/problemas-list.component';
import { ProblemasEditComponent } from '../problemas-edit/problemas-edit.component';
import { ProblemasViewComponent } from '../problemas-view/problemas-view.component';

import { ExamenesListComponent } from '../examenes-list/examenes-list.component';
import { ExamenesAddComponent } from '../examenes-add/examenes-add.component';
import { ExamenesEditComponent } from '../examenes-edit/examenes-edit.component';
import { ExamenesDetailComponent } from '../examenes-detail/examenes-detail.component';

import { CirculosListComponent } from '../circulos-list/circulos-list.component';
import { CirculosAddComponent } from '../circulos-add/circulos-add.component';
import { CirculosEditComponent } from '../circulos-edit/circulos-edit.component';
import { CirculosDetailComponent } from '../circulos-detail/circulos-detail.component';

import { DataService } from '../../services/data.service';
import { MessageService } from '../../services/message.service';
import { AlertService } from '../../services/alert.service';

import { AppRoutingModule } from '../../helpers/app-routing.module';
import { JwtInterceptor } from '../../helpers/jwt.interceptor';
import { AuthGuard } from '../../helpers/auth.guard';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NavbarComponent,
        MessagesComponent,
        LoginComponent,
        AlertComponent,
        ProblemasAddComponent,
        ProblemasListComponent,
        ProblemasEditComponent,
        ProblemasViewComponent,
        ExamenesListComponent,
        ExamenesAddComponent,
        ExamenesEditComponent,
        ExamenesDetailComponent,
        CirculosListComponent,
        CirculosAddComponent,
        CirculosEditComponent,
        CirculosDetailComponent
      ],
      imports: [
        BrowserModule,
        ReactiveFormsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        NgbModule.forRoot(),
        AngularFontAwesomeModule
      ],
      providers: [
        AuthGuard,
        DataService,
        MessageService,
        AlertService,
        { provide: APP_BASE_HREF, useValue : '/' },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptor,
          multi: true
        },
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
