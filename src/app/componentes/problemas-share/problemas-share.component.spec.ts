import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemasShareComponent } from './problemas-share.component';

describe('ProblemasShareComponent', () => {
  let component: ProblemasShareComponent;
  let fixture: ComponentFixture<ProblemasShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemasShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemasShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
