import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Circulo } from '../../models/circulo';
import { Problema } from '../../models/problema';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-problemas-share',
  templateUrl: './problemas-share.component.html',
  styleUrls: ['./problemas-share.component.css']
})
export class ProblemasShareComponent implements OnInit {

  @Input() id: number;
  circulos: Circulo[];
  circulosProblema: Circulo[];

  constructor(
    public activeModal: NgbActiveModal,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.getCirculos();
  }

  getCirculos(): void {
    const id = this.id;
    this.dataService.getProblema(id)
      .subscribe(problema => {
        this.circulosProblema = problema.compartido;
        this.dataService.getCirculos()
          .subscribe(circulos => {
            this.circulos = circulos
            this.circulos.sort(function(obj1, obj2) {
              return obj1.id - obj2.id;
            });
          });
      })
  }

  check(circulo: Circulo) {
    return this.circulosProblema.some(other => circulo.id === other.id);
  }

  onChange(circulo: Circulo) {
    var element = <HTMLInputElement>document.getElementById(String(circulo.id));
    if (element.checked){
      this.dataService.addProblemaCirculo(circulo.id, this.id).subscribe(data => this.getCirculos());
    }
    else{
      this.dataService.removeProblemaCirculo(circulo.id, this.id).subscribe(data => this.getCirculos());
    }
  }

}
