import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Problema } from '../../models/problema';
import { Pregunta } from '../../models/pregunta';
import { Profesor } from '../../models/profesor';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-problemas-edit',
  templateUrl: './problemas-edit.component.html',
  styleUrls: ['./problemas-edit.component.css']
})
export class ProblemasEditComponent implements OnInit {

  problemaForm: FormGroup;
  problema: Problema;

  formErrors = {
    'resumen': '',
    'tags': ''
  };

  validationMessages = {
    'resumen': {
      'required': 'El campo resumen es obligatorio.',
      'maxlength': 'El resumen no puede tener más de 60 caracteres.'
    },
    'tags': {
      'required': 'El campo tags es obligatorio.',
      'pattern': 'Los tags tienen que ir separados por un único espacio.',
    }
  };

  constructor(
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private dataService: DataService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.getProblema();
  }

  createForm() {
    this.problemaForm = this.fb.group({
      resumen: ['', [
        Validators.required,
        Validators.maxLength(60)
      ]],
      enunciadoGeneral: [''],
      tags: ['', [
        Validators.required,
        Validators.pattern('(\\S+ )*\\S+')
      ]],
      preguntas: this.fb.array([]),
    });

    this.problemaForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now 
  }

  onValueChanged(data?: any) {
    if (!this.problemaForm) { return; }
    const form = this.problemaForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  getProblema(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getProblema(id)
      .subscribe(
        problema => {
          if (!problema.es_borrable) {
            //this.gotoList();
            this.alertService.error("La edicción de este problema no está disponible, compruebe que el problema no está en un examen cerrado o publicado de cualquier usuario");
          }
          else {
            this.problema = problema;
            this.rebuildForm(problema);
          }
        },
        error => {
          this.gotoList();
          //this.alertService.error(error.status);
          /*
          if (error.status == 403) {
            this.alertService.error("");
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }*/
        });
  }

  rebuildForm(problema: Problema) {
    this.problemaForm.reset({
      resumen: problema.resumen,
      enunciadoGeneral: problema.enunciado,
      tags: problema.tags.join(' ')
    });
    this.setPreguntas(problema.cuestiones);
  }

  setPreguntas(preguntas: Pregunta[]) {
    const preguntaFGs = preguntas.map(pregunta => this.fb.group(pregunta));
    const preguntaFormArray = this.fb.array(preguntaFGs);
    this.problemaForm.setControl('preguntas', preguntaFormArray);
  }

  createPregunta(): FormGroup {
    return this.fb.group({
      puntos: 1,
      enunciado: '',
      respuesta: '',
      explicacion: ''
    });
  }

  get preguntas(): FormArray {
    return this.problemaForm.get('preguntas') as FormArray;
  };

  addPregunta(): void {
    this.preguntas.push(this.createPregunta());
    this.problemaForm.markAsDirty();
  }

  removePregunta(index: number) {
    this.preguntas.removeAt(index);
    this.problemaForm.markAsDirty();
  }

  moveDown(index: number) {
    if (index < this.preguntas.controls.length - 1) {
      this.swap(index, index + 1)
    }
  }

  moveUp(index: number) {
    if (index >= 1) {
      this.swap(index, index - 1)
    }
  }

  private swap(x: any, y: any) {

    const preguntasDeepCopy: Pregunta[] = this.problemaForm.value.preguntas.map(
      (pregunta: Pregunta) => Object.assign({}, pregunta)
    );

    var b = preguntasDeepCopy[x];
    console.log(b)
    preguntasDeepCopy[x] = preguntasDeepCopy[y];
    preguntasDeepCopy[y] = b;

    this.setPreguntas(preguntasDeepCopy);

    this.problemaForm.markAsDirty();
  }

  reset() {
    this.getProblema();
  }


  onSubmit() {
    let problema = this.prepareSaveProblema();
    this.dataService.updateProblema(problema)
      .subscribe(
        any => {
          this.goBack();
        },
        error => {
          if (error.status == 422) {
            this.alertService.error("La edicción de este problema no está disponible, compruebe que el problema no está en un examen cerrado o publicado de cualquier usuario");
          }
          else {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
  }

  prepareSaveProblema(): Problema {
    const formModel = this.problemaForm.value;

    // deep copy de las preguntas del form model
    const preguntasDeepCopy: Pregunta[] = formModel.preguntas.map(
      (pregunta: Pregunta) => Object.assign({}, pregunta)
    );

    const tagsArray: string[] = formModel.tags.toString().split(' ').filter(function (el) { return el.length != 0 });

    // return new `Problema` object containing a combination of original hero value(s)
    // and deep copies of changed form model values
    const id = +this.route.snapshot.paramMap.get('id');
    const saveProblema: Problema = {
      id: id,
      resumen: formModel.resumen as string,
      enunciado: formModel.enunciadoGeneral as string,
      tags: tagsArray,
      cuestiones: preguntasDeepCopy,
      n_cuestiones: preguntasDeepCopy.length,
      puntos: 0,
      creador: new Profesor(),
      es_borrable: true,
      es_compartible: true,
      compartido: []
    };
    return saveProblema;
  }

  goBack() {
    this.location.back();
  }
  gotoList() {
    this.router.navigate(['/problemas-list']);
  }
}
