import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemasEditComponent } from './problemas-edit.component';

describe('ProblemasEditComponent', () => {
  let component: ProblemasEditComponent;
  let fixture: ComponentFixture<ProblemasEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemasEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemasEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
