import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../../services/data.service';

import { Examen } from '../../models/examen';
import { Problema } from '../../models/problema';

@Component({
  selector: 'app-examenes-add',
  templateUrl: './examenes-add.component.html',
  styleUrls: ['./examenes-add.component.css']
})


export class ExamenesAddComponent implements OnInit {

  examen = new Examen();
  examenForm: FormGroup;
  estados = ['abierto'];
  
  formErrors = {
    'asignatura': '',
    'titulacion': '',
    'fecha': '',
    'convocatoria': ''
  };

  validationMessages = {
    'asignatura': {
      'required': 'El campo asignatura es obligatorio.'
    },
    'titulacion': {
      'required': 'El campo titulacion es obligatorio.'
    },
    'convocatoria': {
      'required': 'El campo convocatoria es obligatorio.'
    },
    'fecha': {
      'required': 'El campo fecha es obligatorio.'
    }
  };

  constructor(
    private dataService: DataService,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.examenForm = this.fb.group({
      asignatura: ['', Validators.required],
      titulacion: ['', Validators.required],
      fecha: ['', Validators.required],
      convocatoria: ['', Validators.required],      
      intro: '',
      estado: 'abierto'
    });

    this.examenForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now 
  }

  onValueChanged(data?: any) {
    if (!this.examenForm) { return; }
    const form = this.examenForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  reset() {
    this.createForm();
  }

  
  //Handle create examen
  onSubmit() {
    if (this.examenForm.invalid) {
      return; //Validation failed, exit from method.
    }
    
    //Form is valid, now perform create or update
    let examen = this.prepareSaveExamen();
    //Create examen
    this.dataService.addExamen(examen)
    .subscribe(() => this.gotoList());
  }

  prepareSaveExamen(): Examen {
    const formModel = this.examenForm.value;

    const fecha: string = (formModel.fecha as string).replace(/\-/g,'');

    // return new `Examen` object containing a combination of original examen value(s)
    // and deep copies of changed form model values
    const saveExamen: Examen = {
      id: this.examen.id,
      estado: formModel.estado as string,
      asignatura: formModel.asignatura as string,
      titulacion: formModel.titulacion as string,
      fecha: fecha,
      convocatoria: formModel.convocatoria as string,
      intro: formModel.intro as string,
      problemas: this.examen.problemas,
      total_puntos: 0   // No usado por el servidor
    };
    return saveExamen;
  }

  gotoList() {
    this.router.navigate(['/examenes-list']);
  }
}
