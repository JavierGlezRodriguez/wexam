import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { Examen } from '../../models/examen';

import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';
import { PagerService } from '../../services/pager.service';

import { ExamenesDownloadComponent } from '../examenes-download/examenes-download.component';


@Component({
  selector: 'app-examenes-list',
  templateUrl: './examenes-list.component.html',
  styleUrls: ['./examenes-list.component.css']
})
export class ExamenesListComponent implements OnInit {

  examenes: Examen[];
  examenesFiltrados: Examen[];

  // pager object
  pager: any = {};
  // current page of items
  pageOfExamenes: Examen[];

  buscar: string = sessionStorage.getItem('buscar_examenesList');
  campo: string = sessionStorage.getItem('campo_examenesList');
  campos = ['todo', 'estado', 'asignatura', 'titulacion', 'convocatoria'];

  constructor(
    private alertService: AlertService,
    private dataService: DataService,
    private modalService: NgbModal,
    private pagerService: PagerService
  ) { }

  ngOnInit() {
    this.getExamenes();
  }

  getExamenes(): void {
    this.dataService.getExamenes()
      .subscribe(
        examenes => {
          this.examenes = examenes;
          this.assignCopy();
          this.search(this.buscar, this.campo);
        },
        error => {
          if (error.status == 0) {
            this.alertService.error("Error desconocido, es posible que el servidor esté caido");
          }
        });
  }

  delete(examen: Examen): void {
    if (confirm("¿Está seguro de querer borrar " + examen.asignatura + " / " + examen.titulacion + " / " + examen.convocatoria + " ?")) {
      this.examenes = this.examenes.filter(h => h !== examen);
      this.dataService.deleteExamen(examen).subscribe(data => this.search(this.buscar, this.campo));
    }
  }

  downloadExamen(examen: Examen, formato: string): void {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false
    };
    const modalRef = this.modalService.open(ExamenesDownloadComponent, ngbModalOptions);
    modalRef.componentInstance.id = examen.id;
    modalRef.componentInstance.formato = formato;
  }

  assignCopy() {
    this.examenesFiltrados = Object.assign([], this.examenes);
    this.setPage(1);
  }

  search(buscar, campo) {
    if (!buscar) this.assignCopy(); //when nothing has typed

    if (buscar != null && campo != null) {
      sessionStorage.setItem('buscar_examenesList', buscar);
      sessionStorage.setItem('campo_examenesList', campo);
      this.buscar = buscar;
      this.campo = campo;
    }
    else{
      sessionStorage.setItem('buscar_examenesList', "");
      sessionStorage.setItem('campo_examenesList', "todo");
      this.buscar = "";
      this.campo = "todo";
    }

    if (campo == 'todo') {
      this.examenesFiltrados = Object.assign([], this.examenes).filter(
        examen => buscar.toLowerCase().split(" ").every((elem) => examen.estado.toLowerCase().includes(elem) ||
          examen.asignatura.toLowerCase().includes(elem) ||
          examen.titulacion.toLowerCase().includes(elem) ||
          examen.convocatoria.toLowerCase().includes(elem)
        )
      )
    }

    if (campo == 'estado') {
      this.examenesFiltrados = Object.assign([], this.examenes).filter(
        examen => buscar.toLowerCase().split(" ").every((elem) => examen.estado.toLowerCase().includes(elem)))
    }
    if (campo == 'asignatura') {
      this.examenesFiltrados = Object.assign([], this.examenes).filter(
        examen => buscar.toLowerCase().split(" ").every((elem) => examen.asignatura.toLowerCase().includes(elem)))
    }
    if (campo == 'titulacion') {
      this.examenesFiltrados = Object.assign([], this.examenes).filter(
        examen => buscar.toLowerCase().split(" ").every((elem) => examen.titulacion.toLowerCase().includes(elem)))
    }
    if (campo == 'convocatoria') {
      this.examenesFiltrados = Object.assign([], this.examenes).filter(
        examen => buscar.toLowerCase().split(" ").every((elem) => examen.convocatoria.toLowerCase().includes(elem)))
    }

    this.setPage(1);
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.examenesFiltrados.length, page);

    // get current page of items
    this.pageOfExamenes = this.examenesFiltrados.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  spin(event){
    event.target.classList.add('fa-spin'); // To ADD
    setTimeout(function() { event.target.classList.remove('fa-spin'); }, 1000);
  }
}
