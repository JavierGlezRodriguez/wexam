import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenesViewComponent } from './examenes-view.component';

describe('ExamenesViewComponent', () => {
  let component: ExamenesViewComponent;
  let fixture: ComponentFixture<ExamenesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
