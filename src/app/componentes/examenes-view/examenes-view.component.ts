import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { Examen } from '../../models/examen';

import { DataService } from '../../services/data.service';

import { ExamenesDownloadComponent } from '../examenes-download/examenes-download.component';

@Component({
  selector: 'app-examenes-view',
  templateUrl: './examenes-view.component.html',
  styleUrls: ['./examenes-view.component.css']
})
export class ExamenesViewComponent implements OnInit {

  examen: Examen;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.getExamen();
  }

  getExamen(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getExamen(id)
      .subscribe(examen => {
        this.examen = examen;
        this.examen.fecha = this.formatFecha(examen.fecha);
        this.examen.total_puntos = this.examen.problemas.reduce((a, b) => a + b.puntos, 0);
      });
  }

  formatFecha(fecha: string): string {
    fecha = [fecha.slice(0, 4), "-", fecha.slice(4)].join('');
    fecha = [fecha.slice(0, 7), "-", fecha.slice(7)].join('');
    var fechaArray = new Array;
    fechaArray = fecha.split("-");
    return fecha = fechaArray[2]+"-"+fechaArray[1]+"-"+fechaArray[0];
  }

  delete(examen: Examen): void {
    if (confirm("¿Está seguro de querer borrar " + examen.asignatura + " / " + examen.titulacion + " / " + examen.convocatoria + " ?")) {
      this.dataService.deleteExamen(examen).subscribe(any => this.gotoList());
    }
  }

  downloadExamen(examen: Examen, formato: string): void {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false
    };
    const modalRef = this.modalService.open(ExamenesDownloadComponent, ngbModalOptions);
    modalRef.componentInstance.id = examen.id;
    modalRef.componentInstance.formato = formato;
  }

  gotoList() {
    this.router.navigate(['/examenes-list']);
  }

}
