import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenesDownloadComponent } from './examenes-download.component';

describe('ExamenesDownloadComponent', () => {
  let component: ExamenesDownloadComponent;
  let fixture: ComponentFixture<ExamenesDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenesDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenesDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
