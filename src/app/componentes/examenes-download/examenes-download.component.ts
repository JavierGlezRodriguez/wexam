import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-examenes-download',
  templateUrl: './examenes-download.component.html',
  styleUrls: ['./examenes-download.component.css']
})
export class ExamenesDownloadComponent implements OnInit {

  @Input() id: number;
  @Input() formato: string;
  selectedValue = "Sin respuestas";
  link;

  constructor(
    public activeModal: NgbActiveModal,
    public dataService: DataService
  ) { }

  ngOnInit() {
  }

  iniciarDescarga() {

    var div = document.getElementById('select');
    div.style.display = 'none';
    div = document.getElementById('info');
    div.style.display = 'none';
    div = document.getElementById('procesando');
    div.style.display = 'inline';

    this.dataService.startDownloadingExamen(this.id, this.formato, this.selectedValue).subscribe(
      data => {
        var intervalId = setInterval(() => {
          this.procesarDescarga(data.link.replace(/http:/, "https:"), data.status, intervalId);
        }, 3000);
      });
  }

  procesarDescarga(link: string, status: string, intervalId) {
    this.dataService.continueDownloadingExamen(link).subscribe(
      data => {
        if (data.status == "Completada") {
          this.link = data.link.replace(/http:/, "https:");
          clearInterval(intervalId);
          var div = document.getElementById('procesando');
          div.style.display = 'none';
          div = document.getElementById('terminado');
          div.style.display = 'inline';
        }
        if (data.status == "Fallida") {
          clearInterval(intervalId);
          var div = document.getElementById('procesando');
          div.style.display = 'none';
          div = document.getElementById('error');
          div.style.display = 'inline';
        }
      });
  }

  terminarDescarga() {
    this.dataService.finishDownloadingExamen(this.link).subscribe(
      data => {
        this.descargarArchivo(data);
      }
    )
  }

  descargarArchivo(data: Response) {
    if (this.formato == "pdf") {
      var blob = new Blob([data], { type: 'application/pdf' });
    }
    else {
      var blob = new Blob([data], { type: 'application/zip' });
    }
    var url = window.URL.createObjectURL(blob);

    var save = document.createElement('a');
    save.href = url;
    save.name = 
    save.target = '_blank';
    save.download = 'examen.'+this.formato;
    document.body.appendChild(save);
    save.click();
    document.body.removeChild(save);

    this.activeModal.dismiss('Descarga terminada')
  }

}
