import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


import { DataService } from '../../services/data.service';

import { Examen } from '../../models/examen';
import { Problema } from '../../models/problema';

@Component({
  selector: 'app-examenes-edit',
  templateUrl: './examenes-edit.component.html',
  styleUrls: ['./examenes-edit.component.css']
})
export class ExamenesEditComponent implements OnInit {

  examenForm: FormGroup;
  estados = ['abierto', 'cerrado', 'publicado'];

  formErrors = {
    'asignatura': '',
    'titulacion': '',
    'fecha': '',
    'convocatoria': '',
  };

  validationMessages = {
    'asignatura': {
      'required': 'El campo asignatura es obligatorio.'
    },
    'titulacion': {
      'required': 'El campo titulacion es obligatorio.'
    },
    'convocatoria': {
      'required': 'El campo convocatoria es obligatorio.'
    },
    'fecha': {
      'required': 'El campo fecha es obligatorio.'
    }
  };

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private location: Location,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.getExamen();
  }

  createForm() {
    this.examenForm = this.fb.group({
      asignatura: ['', Validators.required],
      titulacion: ['', Validators.required],
      fecha: ['', Validators.required],
      convocatoria: ['', Validators.required],
      intro: '',
      estado: ['', Validators.required]
    });

    this.examenForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now 
  }

  onValueChanged(data?: any) {
    if (!this.examenForm) { return; }
    const form = this.examenForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  getExamen(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getExamen(id)
      .subscribe(examen => this.rebuildForm(examen));
  }

  rebuildForm(examen: Examen) {
    if (examen==null || examen.estado=="publicado") {
      this.gotoList();
    }

    var fecha = [examen.fecha.slice(0, 4), "-", examen.fecha.slice(4)].join('');
    fecha = [fecha.slice(0, 7), "-", fecha.slice(7)].join('');

    this.examenForm.reset({
      asignatura: examen.asignatura,
      titulacion: examen.titulacion,
      fecha: fecha,
      convocatoria: examen.convocatoria,
      intro: examen.intro,
      estado: examen.estado
    });
  }

  reset() {
    this.getExamen();
  }

  //Handle create examen
  onSubmit() {
    if (this.examenForm.invalid) {
      return; //Validation failed, exit from method.
    }

    //Form is valid, now perform create or update
    let examen = this.prepareSaveExamen();

    if (examen.estado == "publicado"){
      if (confirm("El estado del examen es " + examen.estado + ". No podrá volver a modificarlo ¿Está seguro?")) {
        //Create examen
        this.dataService.updateExamen(examen)
          .subscribe(() => this.gotoList());
      }
    }
  
    else{
      this.dataService.updateExamen(examen)
          .subscribe(() => this.goBack());
    }
    
  }

  prepareSaveExamen(): Examen {
    const formModel = this.examenForm.value;
    const fecha: string = (formModel.fecha as string).replace(/\-/g, '');
    const id = +this.route.snapshot.paramMap.get('id');
    const problemas: Problema[] = [];

    // return new `Examen` object containing a combination of original examen value(s)
    // and deep copies of changed form model values
    const saveExamen: Examen = {
      id: id,
      estado: formModel.estado as string,
      asignatura: formModel.asignatura as string,
      titulacion: formModel.titulacion as string,
      fecha: fecha,
      convocatoria: formModel.convocatoria as string,
      intro: formModel.intro as string,
      problemas: problemas,
      total_puntos: 0        // No usado por el servidor

    };
    return saveExamen;
  }

  goBack() {
    this.location.back();  
  }

  gotoList() {
    this.router.navigate(['/examenes-list']);
  }

}
