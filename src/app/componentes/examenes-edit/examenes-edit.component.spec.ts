import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenesEditComponent } from './examenes-edit.component';

describe('ExamenesEditComponent', () => {
  let component: ExamenesEditComponent;
  let fixture: ComponentFixture<ExamenesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
