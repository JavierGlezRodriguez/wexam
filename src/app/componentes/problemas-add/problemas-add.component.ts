import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import { Problema } from '../../models/problema';
import { Pregunta } from '../../models/pregunta';
import { Profesor } from '../../models/profesor';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-problemas-add',
  templateUrl: './problemas-add.component.html',
  styleUrls: ['./problemas-add.component.css']
})
export class ProblemasAddComponent implements OnInit {

  problema = new Problema();
  problemaForm: FormGroup;

  formErrors = {
    'resumen': '',
    'tags': ''
  };

  validationMessages = {
    'resumen': {
      'required': 'El campo resumen es obligatorio.',
      'maxlength': 'El resumen no puede tener más de 60 caracteres.'
    },
    'tags': {
      'required': 'El campo tags es obligatorio.',
      'pattern': 'Los tags tienen que ir separados por un único espacio.'
    }
  };

  constructor(
    private dataService: DataService,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.problemaForm = this.fb.group({
      resumen: ['', [
        Validators.required,
        Validators.maxLength(60)
      ]],
      enunciadoGeneral: [''],
      tags: ['', [
        Validators.required,
        Validators.pattern('(\\S+ )*\\S+') 
      ]],
      preguntas: this.fb.array([this.createPregunta()])
    });

    this.problemaForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now 
  }

  onValueChanged(data?: any) {
    if (!this.problemaForm) { return; }
    const form = this.problemaForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  createPregunta(): FormGroup {
    return this.fb.group({
      puntos: 1,
      enunciado: '',
      respuesta: '',
      explicacion: ''
    });
  }

  
  get preguntas(): FormArray {
    return this.problemaForm.get('preguntas') as FormArray;
  };

  addPregunta(): void {
    this.preguntas.push(this.createPregunta());
  }

  removePregunta(index: number) {
    this.preguntas.removeAt(index);
  }

  moveDown(index: number) {
    if (index < this.preguntas.controls.length - 1) {
      this.swap(index, index + 1)
    }
  }

  moveUp(index: number) {
    if (index >= 1) {
      this.swap(index, index - 1)
    }
  }

  private swap(x: any, y: any) {

    const preguntasDeepCopy: Pregunta[] = this.problemaForm.value.preguntas.map(
      (pregunta: Pregunta) => Object.assign({}, pregunta)
    );

    var b = preguntasDeepCopy[x];
    console.log(b)
    preguntasDeepCopy[x] = preguntasDeepCopy[y];
    preguntasDeepCopy[y] = b;

    this.setPreguntas(preguntasDeepCopy);
    
    this.problemaForm.markAsDirty();
  }

  setPreguntas(preguntas: Pregunta[]) {
    const preguntaFGs = preguntas.map(pregunta => this.fb.group(pregunta));
    const preguntaFormArray = this.fb.array(preguntaFGs);
    this.problemaForm.setControl('preguntas', preguntaFormArray);
  }

  reset() {
    this.createForm();
  }

  //Handle create problema
  onSubmit() {
    if (this.problemaForm.invalid) {
      return; //Validation failed, exit from method.
    }
    //Form is valid, now perform create or update
    let problema = this.prepareSaveProblema();
    this.dataService.addProblema(problema)
    .subscribe(() => this.gotoList());
  }

  prepareSaveProblema(): Problema {
    const formModel = this.problemaForm.value;

    // deep copy de las preguntas del form model
    const preguntasDeepCopy: Pregunta[] = formModel.preguntas.map(
      (pregunta: Pregunta) => Object.assign({}, pregunta)
    );

    const tagsArray: string[] = (formModel.tags as string).split(' ').filter(function (el) { return el.length != 0 });

    // return new `Problema` object containing a combination of original problema value(s)
    // and deep copies of changed form model values
    const saveProblema: Problema = {
      id: this.problema.id,
      resumen: formModel.resumen as string,
      enunciado: formModel.enunciadoGeneral as string,
      tags: tagsArray,
      cuestiones: preguntasDeepCopy,
      n_cuestiones: preguntasDeepCopy.length,
      puntos: 0,
      creador: new Profesor(),
      es_borrable: true,
      es_compartible: true,
      compartido: []
    };
    return saveProblema;
  }

  gotoList() {
    this.router.navigate(['/problemas-list']);
  }

}
