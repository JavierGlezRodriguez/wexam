import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemasAddComponent } from './problemas-add.component';

describe('ProblemasAddComponent', () => {
  let component: ProblemasAddComponent;
  let fixture: ComponentFixture<ProblemasAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProblemasAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProblemasAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
