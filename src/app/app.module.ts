import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MarkdownModule } from 'ngx-markdown';

// Componente raíz
import { AppComponent } from './componentes/app/app.component';

// Componentes del login
import { LoginComponent } from './componentes/login/login.component';
import { LoginResetPasswordComponent } from './componentes/login-reset-password/login-reset-password.component';
import { AlertComponent } from './componentes/alert/alert.component';

// Componentes que aparecen en otros componentes: Barra de navegación y mensajes de depuración
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { MessagesComponent } from './componentes/messages/messages.component';

// Componentes de problemas
import { ProblemasAddComponent } from './componentes/problemas-add/problemas-add.component';
import { ProblemasListComponent } from './componentes/problemas-list/problemas-list.component';
import { ProblemasEditComponent } from './componentes/problemas-edit/problemas-edit.component';
import { ProblemasViewComponent } from './componentes/problemas-view/problemas-view.component';
import { ProblemasShareComponent } from './componentes/problemas-share/problemas-share.component';

// Componentes de examen
import { ExamenesListComponent } from './componentes/examenes-list/examenes-list.component';
import { ExamenesAddComponent } from './componentes/examenes-add/examenes-add.component';
import { ExamenesEditComponent } from './componentes/examenes-edit/examenes-edit.component';
import { ExamenesDetailComponent } from './componentes/examenes-detail/examenes-detail.component';
import { ExamenesDownloadComponent } from './componentes/examenes-download/examenes-download.component';
import { ExamenesViewComponent } from './componentes/examenes-view/examenes-view.component';

// Componentes de círculos
import { CirculosListComponent } from './componentes/circulos-list/circulos-list.component';
import { CirculosAddComponent } from './componentes/circulos-add/circulos-add.component';
import { CirculosEditComponent } from './componentes/circulos-edit/circulos-edit.component';
import { CirculosDetailComponent } from './componentes/circulos-detail/circulos-detail.component';

// Servicios
import { DataService } from './services/data.service';
import { PagerService } from './services/pager.service';
import { MessageService } from './services/message.service';
import { AlertService } from './services/alert.service';

// Helpers
import { AppRoutingModule } from './helpers/app-routing.module';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { AuthGuard } from './helpers/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MessagesComponent,
    LoginComponent,
    LoginResetPasswordComponent,
    AlertComponent,
    ProblemasAddComponent,
    ProblemasListComponent,
    ProblemasEditComponent,
    ProblemasViewComponent,
    ProblemasShareComponent,
    ExamenesListComponent,
    ExamenesAddComponent,
    ExamenesEditComponent,
    ExamenesDetailComponent,
    ExamenesDownloadComponent,
    ExamenesViewComponent,
    CirculosListComponent,
    CirculosAddComponent,
    CirculosEditComponent,
    CirculosDetailComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    MarkdownModule.forRoot(),
    AngularFontAwesomeModule
  ],
  providers: [
    AuthGuard,
    DataService,
    PagerService,
    MessageService,
    AlertService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginResetPasswordComponent,
    ProblemasShareComponent,
    ExamenesDownloadComponent,
    CirculosAddComponent,
    CirculosEditComponent
  ]
})
export class AppModule { }
