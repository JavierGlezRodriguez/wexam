import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Problema } from '../models/problema';
import { Examen } from '../models/examen';
import { Circulo } from '../models/circulo';
import { Profesor } from '../models/profesor';

import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class DataService {

  protected basePath = '/wexam-api/';
  //protected basePath = 'https://atc33.edv.uniovi.es/wexam-api/';
  //protected basePath = 'https://wexam-server.serveo.net';
  public defaultHeaders = new HttpHeaders();


  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
    //this.getConfig().subscribe(data => this.basePath = data.server_name, error => console.log(error));
  }
  
  getConfig(): Observable<any> {
    return this.http.get("../../assets/config.json").pipe(
      tap(data => this.log(`fetched server_name`))
    );
  }

  login(email: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.basePath}/login`, { email: email, password: password }, httpOptions)
      .pipe(
        map(token => {
          // compruebo que recibo el token
          if (token) {
            // store user details and jwt token in session storage to keep user logged in between page refreshes
            sessionStorage.setItem('token', JSON.stringify(token));
          }
          return token;
        }),
        tap(token => this.log(`fetched login token=${token}`))
        //catchError(this.handleError('login'))
      );
  }

  logout() {
    // borrar token del sessionStorage
    sessionStorage.clear();
  }

  resetPassword(email: string) {
    return this.http.get<any>(`${this.basePath}/reset_password/${email}`, httpOptions)
      .pipe(
        tap(data => this.log(`mail sent`))
        //catchError(this.handleError('resetPassword', []))
      );
  }

  /** GET problemas from the server */
  getProblemas(): Observable<Problema[]> {
    return this.http.get<Problema[]>(`${this.basePath}/problemas?orden=fecha_modificacion&reverse=1`, httpOptions)
      .pipe(
        tap(problemas => this.log(`fetched problemas`))
        //catchError(this.handleError('getProblemas', []))
      );
  }

  /** GET problema by id. Will 404 if id not found */
  getProblema(id: number): Observable<Problema> {
    const url = `${this.basePath}/problema/${id}/full`;
    return this.http.get<Problema>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched problema id=${id}`))
      //catchError(this.handleError<Problema>(`getProblema id=${id}`))
    );
  }

  /** POST: add a new problema to the server */
  addProblema(problema: Problema): Observable<Problema> {
    return this.http.post<Problema>(`${this.basePath}/problemas`, problema, httpOptions).pipe(
      tap((problema: Problema) => this.log(`added problema w/ id=${problema.id}`)),
      catchError(this.handleError<Problema>('addProblema'))
    );
  }

  /** PUT: update the problema on the server */
  updateProblema(problema: Problema): Observable<any> {
    const id = typeof problema === 'number' ? problema : problema.id;
    const url = `${this.basePath}/problema/${id}`;

    return this.http.put(url, problema, httpOptions).pipe(
      tap(_ => this.log(`updated problema id=${problema.id}`))
      //catchError(this.handleError<any>('updateProblema'))
    );
  }

  /** DELETE: delete the problema from the server */
  deleteProblema(problema: Problema | number): Observable<Problema> {
    const id = typeof problema === 'number' ? problema : problema.id;
    const url = `${this.basePath}/problema/${id}`;

    return this.http.delete<Problema>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted problema id=${id}`))
      //catchError(this.handleError<Problema>('deleteProblema'))
    );
  }

  /** DELETE: delete the problema from the server */
  cloneProblema(problema: Problema | number): Observable<Problema> {
    const id = typeof problema === 'number' ? problema : problema.id;
    const url = `${this.basePath}/problema/${id}/clone`;

    return this.http.post<Problema>(url, httpOptions).pipe(
      tap(_ => this.log(`clone problema id=${id}`))
      //catchError(this.handleError<Problema>('cloneProblema'))
    );
  }

  /* GET problemas whose resumen contains search term 
  searchProblema(term: string): Observable<Problema[]> {
    if (!term.trim()) {
      // if not search term, return empty problema array.
      return of([]);
    }
    return this.http.get<Problema[]>(`api/problemas/?resumen=${term}`).pipe(
      tap(_ => this.log(`found problemas matching "${term}"`)),
      catchError(this.handleError<Problema[]>('searchProblemas', []))
    );
  }
  */

  /** GET examenes from the server */
  getExamenes(): Observable<Examen[]> {
    return this.http.get<Examen[]>(`${this.basePath}/examenes`, httpOptions)
      .pipe(
        tap(examenes => this.log(`fetched examenes`))
        //catchError(this.handleError('getExamenes', []))
      );
  }

  /** GET examen by id. Will 404 if id not found */
  getExamen(id: number): Observable<Examen> {
    const url = `${this.basePath}/examen/${id}/full`;
    return this.http.get<Examen>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched examen id=${id}`)),
      catchError(this.handleError<Examen>(`getExamen id=${id}`))
    );
  }

  /** POST: add a new examen to the server */
  addExamen(examen: Examen): Observable<Examen> {
    return this.http.post<Examen>(`${this.basePath}/examenes`, examen, httpOptions).pipe(
      tap((examen: Examen) => this.log(`added problema w/ id=${examen.id}`)),
      catchError(this.handleError<Examen>('addExamen'))
    );
  }

  /** PUT: update the examen on the server */
  updateExamen(examen: Examen): Observable<any> {
    const id = typeof examen === 'number' ? examen : examen.id;
    const url = `${this.basePath}/examen/${id}`;

    return this.http.put(url, examen, httpOptions).pipe(
      tap(_ => this.log(`updated examen id=${examen.id}`)),
      catchError(this.handleError<any>('updateExamen'))
    );
  }

  /** DELETE: delete the examen from the server */
  deleteExamen(examen: Examen | number): Observable<Examen> {
    const id = typeof examen === 'number' ? examen : examen.id;
    const url = `${this.basePath}/examen/${id}`;

    return this.http.delete<Examen>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted examen id=${id}`)),
      catchError(this.handleError<Examen>('deleteExamen'))
    );
  }

  /** DOWNLOAD: download the examen from the server */
  startDownloadingExamen(examen: Examen | number, formato: string, resuelto: string): Observable<any> {
    const id = typeof examen === 'number' ? examen : examen.id;
    let url;
    switch (resuelto) {
      case "Sin respuestas": {
        url = `${this.basePath}/examen/${id}/download?formato=${formato}`;
        break;
      }
      case "Con respuestas": {
        url = `${this.basePath}/examen/${id}/download?resuelto=true&formato=${formato}`;
        break;
      }
      case "Resuelto con explicaciones": {
        url = `${this.basePath}/examen/${id}/download?resuelto=explicado&formato=${formato}`;
        break;
      }
    }

    return this.http.get<any>(url, httpOptions).pipe(
      tap(_ => this.log(`downloaded examen id=${id}`))
    );
  }

  continueDownloadingExamen(link: string): Observable<any> {
    return this.http.get<any>(link);
  }

  finishDownloadingExamen(link: string): Observable<any> {
    return this.http.get(link, { responseType: 'blob' });
  }


  /** GET problemas from a examen by id. Will 404 if id not found */
  getProblemasExamen(id: number): Observable<Problema[]> {
    const url = `${this.basePath}/examen/${id}/id_problemas?expanded=1`;
    return this.http.get<Problema[]>(url, httpOptions).pipe(
      tap(problemas => this.log(`fetched problemas`)),
      catchError(this.handleError('getProblemasExamen', []))
    );
  }

  /** POST: add a problema to the examen */
  addProblemaExamen(idExamen: number, idProblema: number): Observable<any> {
    const url = `${this.basePath}/examen/${idExamen}/id_problemas?problema=${idProblema}`;
    return this.http.post<any>(url, httpOptions).pipe(
      tap(problemas => this.log(`added problema id=${idProblema}`)),
      catchError(this.handleError('addProblemaExamen', []))
    );
  }

  /** DELETE: remove a problema from the examen */
  removeProblemaExamen(idExamen: number, idProblema: number): Observable<any> {
    const url = `${this.basePath}/examen/${idExamen}/id_problemas?problema=${idProblema}`;;
    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => this.log(`removed problema id=${idProblema}`)),
      catchError(this.handleError<Circulo>('removeProblemaExamen'))
    );
  }


  /** GET circulos from the server */
  getCirculos(): Observable<Circulo[]> {
    return this.http.get<Circulo[]>(`${this.basePath}/circulos/full`, httpOptions)
      .pipe(
        tap(circulos => this.log(`fetched circulos`))
        //catchError(this.handleError('getCirculo', []))
      );
  }

  /** GET circulo by id. Will 404 if id not found */
  getCirculo(id: number): Observable<Circulo> {
    const url = `${this.basePath}/circulo/${id}`;
    return this.http.get<Circulo>(url, httpOptions).pipe(
      tap(_ => this.log(`fetched circulo id=${id}`)),
      catchError(this.handleError<Circulo>(`getCirculo id=${id}`))
    );
  }

  /** POST: add a new circulo to the server */
  addCirculo(circulo: Circulo): Observable<Circulo> {
    return this.http.post<Circulo>(`${this.basePath}/circulos`, circulo.nombre, httpOptions).pipe(
      tap((circulo: Circulo) => this.log(`added circulo w/ id=${circulo.id}`)),
      catchError(this.handleError<Circulo>('addCirculo'))
    );
  }

  /** PUT: update the circulo on the server */
  updateCirculo(circulo: Circulo): Observable<any> {
    const id = typeof circulo === 'number' ? circulo : circulo.id;
    const url = `${this.basePath}/circulo/${id}`;

    return this.http.put(url, circulo, httpOptions).pipe(
      tap(_ => this.log(`updated circulo id=${circulo.id}`)),
      catchError(this.handleError<any>('updateCirculo'))
    );
  }

  /** DELETE: delete the problema from the server */
  deleteCirculo(circulo: Circulo | number): Observable<Circulo> {
    const id = typeof circulo === 'number' ? circulo : circulo.id;
    const url = `${this.basePath}/circulo/${id}`;

    return this.http.delete<Circulo>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted circulo id=${id}`)),
      catchError(this.handleError<Circulo>('deleteCirculo'))
    );
  }

  /** GET miembros from a circulo by id. Will 404 if id not found */
  getMiembros(id: number): Observable<Profesor[]> {
    const url = `${this.basePath}/circulo/${id}/id_miembros?expanded=1`;
    return this.http.get<Profesor[]>(url, httpOptions).pipe(
      tap(miembros => this.log(`fetched miembros`)),
      catchError(this.handleError('getMiembros', []))
    );
  }

  /** POST: add a miembro to the circulo */
  addMiembro(idCirculo: number, idMiembro: number): Observable<any> {
    const url = `${this.basePath}/circulo/${idCirculo}/id_miembros?miembro=${idMiembro}`;
    return this.http.post<any>(url, httpOptions).pipe(
      tap(miembros => this.log(`added miembro id=${idMiembro}`)),
      catchError(this.handleError('addMiembro', []))
    );
  }

  /** DELETE: remove a miembro from the circulo */
  removeMiembro(idCirculo: number, idMiembro: number): Observable<any> {
    const url = `${this.basePath}/circulo/${idCirculo}/id_miembros?miembro=${idMiembro}`;;
    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => this.log(`removed miembro id=${idMiembro}`)),
      catchError(this.handleError<Circulo>('removeMiembro'))
    );
  }

  /** GET profesores from the server */
  getProfesores(): Observable<Profesor[]> {
    return this.http.get<Profesor[]>(`${this.basePath}/profesores`, httpOptions)
      .pipe(
        tap(profesores => this.log(`fetched profesores`)),
        catchError(this.handleError('getProfesores', []))
      );
  }

  /** GET problemas from a circulo by id. Will 404 if id not found */
  getProblemasCirculo(id: number): Observable<Problema[]> {
    const url = `${this.basePath}/circulo/${id}/id_problemas?expanded=1`;
    return this.http.get<Problema[]>(url, httpOptions).pipe(
      tap(problemas => this.log(`fetched problemas`)),
      catchError(this.handleError('getProblemasCirculo', []))
    );
  }

  /** POST: add a problema to the circulo */
  addProblemaCirculo(idCirculo: number, idProblema: number): Observable<any> {
    const url = `${this.basePath}/circulo/${idCirculo}/id_problemas?problema=${idProblema}`;
    return this.http.post<any>(url, httpOptions).pipe(
      tap(problemas => this.log(`added problema id=${idProblema}`)),
      catchError(this.handleError('addProblemaCirculo', []))
    );
  }

  /** DELETE: remove a problema from the circulo */
  removeProblemaCirculo(idCirculo: number, idProblema: number): Observable<any> {
    const url = `${this.basePath}/circulo/${idCirculo}/id_problemas?problema=${idProblema}`;;
    return this.http.delete<any>(url, httpOptions).pipe(
      tap(_ => this.log(`removed problema id=${idProblema}`)),
      catchError(this.handleError<Circulo>('removeProblemaCirculo'))
    );
  }



  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a DataService message with the MessageService */
  private log(message: string) {
    this.messageService.add('DataService: ' + message);
  }

}
